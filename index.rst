.. SPDX-License-Identifier: CC-BY-SA-4.0
   Copyright (C) 2020 Casper Meijn <casper@meijn.net>
   Dit werk is gelicenseerd onder de licentie Creative Commons Naamsvermelding-GelijkDelen 4.0 Internationaal. 
   Ga naar http://creativecommons.org/licenses/by-sa/4.0/ om een kopie van de licentie te kunnen lezen.
   
Nieuwe veiling WiFi
===================
Het lijkt met tof om een WiFi netwerk te bouwen ter grote van de hele wijk. Wat ik voor me zie is dat iedere woning een WiFi router installeert met speciale software. Deze maken met elkaar verbinding en sturen netwerk verkeer naar elkaar door. In sommige woningen is er ook een verbinding met het internet, deze verbinding wordt dan met de anderen gedeeld. Er zijn ook woningen die een dienst aan het netwerk aanbieden, door bijvoorbeeld een Raspberry Pi aan het netwerk aan te sluiten.

Voordelen
---------
Stel we zouden dit maken, wat levert dit nou op? 

- Het is leuk om met techniek bezig te zijn. Met het bouwen en onderhouden van een vrij en open netwerk kan je veel leren. Het modigt ook vrije toegang tot informatie aan.
- We kunnen een gast WiFi netwerk aanbieden, zodat ons bezoek makkelijk op het internet kan.
- Het zal onderdeel zijn van een decentraal internet, waardoor je minder afhankelijk bent van een comerciele internet provider. Mogelijk kunnen we kosten besparen door internet verbindingen te delen.
- Vanaf het netwerk kunnen er diensten worden aangeboden, bijvoorbeeld een buurtchat, een VPN, netwerk schijfruimte of een gedeelde printer. Bij deze diensten wordt je niet gevolgd en hebben we het beheer in eigen hand.

Huidige status
--------------
Tot nu toe zijn dit enkel ideeën. Met deze website hoop ik mensen bij elkaar te krijgen om dit daadwerkelijk te gaan doen.

.. toctree::
   :hidden:
      
   techniek/index
   bestuur/index

