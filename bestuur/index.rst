.. SPDX-License-Identifier: CC-BY-SA-4.0
   Copyright (C) 2020 Casper Meijn <casper@meijn.net>
   Dit werk is gelicenseerd onder de licentie Creative Commons Naamsvermelding-GelijkDelen 4.0 Internationaal. 
   Ga naar http://creativecommons.org/licenses/by-sa/4.0/ om een kopie van de licentie te kunnen lezen.

Bestuur
=======

Er is momenteel nog geen bestuur (noch een organisatie), maar hier zijn enkele bestuurlijke zaken te vinden.


.. toctree::   
   uitnodiging
